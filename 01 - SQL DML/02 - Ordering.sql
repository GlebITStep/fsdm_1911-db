-- Запросы в данном примере к БД BooksSQL, которую вы можете
-- найти в репозитории и подключить в SSMS.

-- Убедитесь, что в SSMS у вас выбрана база данных BooksSQL.
-- BooksSQL - База данных книжного магазина

-- Таблица books состоит из следующих колонок
-- N - Уникальный идентификатор книги
-- Code - Код книги в магазине
-- New - Является ли книга новинкой
-- Name - Название книги
-- Price - Цена
-- Izd - Издательство
-- Pages - Количество страниц
-- Format - Физические размеры книги 
-- Date - Дата издания
-- Pressrun - Тираж
-- Themes - Тема
-- Category - Категория


-- Получить все книги и отсортировать по цене по возрастанию (ASC)
-- Ключевое слово ASC писать не обязательно
SELECT *
FROM [books]
ORDER BY [Price] ASC


-- Сортировка по убыванию (DESC)
SELECT *
FROM [books]
ORDER BY [Price] DESC


-- Отсортировать книги по ценам, а затем по тиражу
-- Книги с одинаковыми ценами отсортируются между собой по тиражу
SELECT *
FROM [books]
ORDER BY [Price] DESC, [Pressrun] ASC


-- Пример математической операции в сортировке
-- Книги отсортируются по общей стоимости всего тиража
SELECT *
FROM [books]
ORDER BY [Price] * [Pressrun] DESC


-- Получить первые 5 книг (TOP 5) из выборки
SELECT TOP 5 *
FROM [books]
ORDER BY [Price] DESC


-- Получить первый процент (TOP 1 PERCENT) книг из выборки
SELECT TOP 1 PERCENT *
FROM [books]
ORDER BY [Price] DESC


-- Сделать отступ в 20 книг (OFFSET 20 ROWS)
-- И забрать следуюшие 10 (FETCH NEXT 10 ROWS ONLY)
SELECT *
FROM [books]
ORDER BY [Price]
OFFSET 20 ROWS
FETCH NEXT 10 ROWS ONLY