-- Запросы в данном примере к БД BooksSQL, которую вы можете
-- найти в репозитории и подключить в SSMS.

-- Убедитесь, что в SSMS у вас выбрана база данных BooksSQL.
-- BooksSQL - База данных книжного магазина

-- Таблица books состоит из следующих колонок
-- N - Уникальный идентификатор книги
-- Code - Код книги в магазине
-- New - Является ли книга новинкой
-- Name - Название книги
-- Price - Цена
-- Izd - Издательство
-- Pages - Количество страниц
-- Format - Физические размеры книги 
-- Date - Дата издания
-- Pressrun - Тираж
-- Themes - Тема
-- Category - Категория



-- Получить все книги с ценой 9
-- Обратите внимание: = а не ==
SELECT *
FROM [books]
WHERE [Price] = 9


-- Получить все книги с ценой не равной 9
-- Вместо <> можно использовать !=
SELECT *
FROM [books]
WHERE [Price] <> 9


-- Получить все книги с ценой не больше 9
-- Остальные операторы такие как > < >= <= работают так же как мы привыкли
SELECT *
FROM [books]
WHERE [Price] !> 9


-- Получить все книги с ценой 9 или 10
-- OR работает как ||
SELECT *
FROM [books]
WHERE [Price] = 9 OR [Price] = 10


-- Получить все книги с ценой 9 и тиражем больше или равном 5000
-- AND работает как &&
SELECT *
FROM [books]
WHERE [Price] = 9 AND [Pressrun] >= 5000


-- Получить все еники, с ценой не равной 9
-- NOT работает как !
SELECT *
FROM [books]
WHERE NOT [Price] = 9


-- Получить все книги с ценой 10 либо 20 либо 30
SELECT *
FROM [books]
WHERE [Price] = 10 OR [Price] = 20 OR [Price] = 30


-- Альтернативный вариант верхнего запроса
-- Такой вариант более предпочтителен
SELECT *
FROM [books]
WHERE [Price] IN (10, 20, 30)


-- Получить все книги с ценой в диапазоне от 5 до 10 включительно
SELECT *
FROM [books]
WHERE [Price] >= 5 AND [Price] <= 10
ORDER BY [Price]


-- Альтернативный вариант верхнего запроса
-- Такой вариант более предпочтителен
SELECT *
FROM [books]
WHERE [Price] BETWEEN 5 AND 10
ORDER BY [Price]


-- Получить все книги от издательства DiaSoft
-- Строки в SQL пишутся в 'одинарных кавычках'
SELECT *
FROM [books]
WHERE [Izd] = 'DiaSoft'


-- Получить все книги от издательства Феникс
-- N в начале строки говорит что эта страка содержит Unicode символы
SELECT *
FROM [books]
WHERE [Izd] = N'Феникс'


-- Получить все книги, выпущеные 23 февраля 2000
SELECT *
FROM [books]
WHERE [Date] = '2000-02-23'


-- Получить все книги, выпущеные в диапазоне указанных дат
SELECT *
FROM [books]
WHERE [Date] BETWEEN '2000-02-01' AND '2000-02-29'
ORDER BY [Date]


-- Пример написания дат в разных форматах
SELECT *
FROM [books]
WHERE [Date] BETWEEN '01-02-2000' AND '2000.02.29'
ORDER BY [Date]


-- Получить все книги, у которых известна дата выпуска
SELECT *
FROM [books]
WHERE [Date] IS NOT NULL
ORDER BY [Date]
