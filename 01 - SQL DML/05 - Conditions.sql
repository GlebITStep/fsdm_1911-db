-- Запросы в данном примере к БД BooksSQL, которую вы можете
-- найти в репозитории и подключить в SSMS.

-- Убедитесь, что в SSMS у вас выбрана база данных BooksSQL.
-- BooksSQL - База данных книжного магазина

-- Таблица books состоит из следующих колонок
-- N - Уникальный идентификатор книги
-- Code - Код книги в магазине
-- New - Является ли книга новинкой
-- Name - Название книги
-- Price - Цена
-- Izd - Издательство
-- Pages - Количество страниц
-- Format - Физические размеры книги 
-- Date - Дата издания
-- Pressrun - Тираж
-- Themes - Тема
-- Category - Категория


-- Получить все книги и вывести в колонку 'Is new book'
-- YES если книга новая (New = 1)
-- NO если нет (New = 0)
-- NO DATA если в New сидит NULL
SELECT [Name], [Price], 
	CASE
		WHEN [New] = 0 THEN 'NO'
		WHEN [New] = 1 THEN 'YES'
		ELSE 'NO DATA'
	END AS 'Is new book'
FROM [books]


-- Получить все книги и вывести в колонку 'Is new book'
-- YES если книга новая (New = 1)
-- NO если нет (New = 0)
-- Пример тернарного выражения (IIF)
SELECT [Name], [Price], IIF([New] = 0, 'NO', 'YES') AS 'Is new book'
FROM [books]


-- В случае, если категория NULL, вывести 'Unknown' в колонку Category
SELECT [Name], ISNULL([Category], 'Unknown') as Category
FROM [books]


-- В случае, если категория NULL, вывести тему в колонку Category
SELECT [Name], ISNULL([Category], [Themes]) as Category
FROM [books]

-- Вывести категорию, либо тему, либо название, либо Unknown
-- В зависимости от того, какое из этих значений первым окажется не NULL
SELECT [Name], COALESCE([Category], [Themes], [Name], 'Unknown') as Category
FROM [books]