-- Запросы в данном примере к БД BooksSQL, которую вы можете
-- найти в репозитории и подключить в SSMS.

-- Убедитесь, что в SSMS у вас выбрана база данных BooksSQL.
-- BooksSQL - База данных книжного магазина

-- Таблица books состоит из следующих колонок
-- N - Уникальный идентификатор книги
-- Code - Код книги в магазине
-- New - Является ли книга новинкой
-- Name - Название книги
-- Price - Цена
-- Izd - Издательство
-- Pages - Количество страниц
-- Format - Физические размеры книги 
-- Date - Дата издания
-- Pressrun - Тираж
-- Themes - Тема
-- Category - Категория


-- Вставить в таблицу books книгу с этими параметрами
-- Последовательность параметров должна соответствовать
-- последовательности колонок в таблице
-- Первая колонка (N) должна быть пропущена
-- так как БД сама автоматически задает идетификатор добавленным элементам
INSERT INTO [books] 
VALUES (1234, 1, 'Test', 100, 'Gleb', 50, NULL, '2020-05-12', 1000, 'C#', 'Programming', NULL)


-- Вставить книгу с явным указанием колонок и их последовательности
-- В остальные колонки запишется значение NULL
INSERT INTO [books] ([Name], [New])
VALUES ('Test 2', 1)


-- Удалить книгу с идентификатором 770
DELETE FROM [books]
WHERE [N] = 770


-- Удалить все книги, начинающиеся на 'Test'
DELETE FROM [books]
WHERE [Name] LIKE 'Test%'


-- Изменить значение New у книги с идетификатором 771 на 0
UPDATE [books]
SET [New] = 0
WHERE [N] = 771


-- Задать новое название издательства всем книгам, начинающимся на 'Test'
UPDATE [books]
SET [Izd] = 'Gleb Skripnikov'
WHERE [Name] LIKE 'Test%'


-- Убрать название издательства всем книгам, начинающимся на 'Test'
UPDATE [books]
SET [Izd] = NULL
WHERE [Name] LIKE 'Test%'


-- Вставить книгу с названием Azərbaycan
-- В БД добавится Az?rbaycan
INSERT INTO [books] ([Name], [New])
VALUES ('Azərbaycan', 1)


-- Вставить книгу с названием Azərbaycan, указав строку в Unicode формате
-- В БД добавится Azərbaycan
INSERT INTO [books] ([Name], [New])
VALUES (N'Azərbaycan', 1)


-- Получить все книги, с названием Azərbaycan
-- Поиск будет проходить по тексту Az?rbaycan вместо Azərbaycan
SELECT *
FROM [books]
WHERE [Name] = 'Azərbaycan'


-- Получить все книги, с названием Azərbaycan, указав строку в Unicode формате
-- Поиск будет проходить по тексту Azərbaycan
SELECT *
FROM [books]
WHERE [Name] = N'Azərbaycan'