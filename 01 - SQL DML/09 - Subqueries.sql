-- Запросы в данном примере к БД BooksSQL, которую вы можете
-- найти в репозитории и подключить в SSMS.

-- Убедитесь, что в SSMS у вас выбрана база данных BooksSQL.
-- BooksSQL - База данных книжного магазина

-- Таблица books состоит из следующих колонок
-- N - Уникальный идентификатор книги
-- Code - Код книги в магазине
-- New - Является ли книга новинкой
-- Name - Название книги
-- Price - Цена
-- Izd - Издательство
-- Pages - Количество страниц
-- Format - Физические размеры книги 
-- Date - Дата издания
-- Pressrun - Тираж
-- Themes - Тема
-- Category - Категория


-- Получить среднее арифметическое цен всех книг в таблице
-- Результатом будет 27.5881
SELECT AVG([Price])
FROM [books]

-- Получить все книги, цена которых
-- больше чем средняя цена среди всех книг
SELECT *
FROM [books]
WHERE [Price] > 27.5881


-- Получить все книги, цена которых
-- больше чем средняя цена среди всех книг
-- Пример с использованием вложенного запроса, или подзапроса (subquery)
SELECT *
FROM [books]
WHERE [Price] > (SELECT AVG([Price]) FROM [books])


-- Получить самую дешевую книгу
-- Неоптимальный вариант, так как сортировка очень дорогая операция
SELECT TOP 1 *
FROM [books]
ORDER BY [Price]


-- Получить самую дешевую книгу
-- Вариант с использованием подзапроса
SELECT *
FROM [books]
WHERE [Price] = (SELECT MIN([Price]) FROM [books])


-- Получить название издательства и цену его самой дешевой книги 
SELECT [Izd], MIN([Price])
FROM [books]
GROUP BY [Izd]


-- Попытка использовать верхний запрос как подзапрос
-- Для получения всех самых дешевых книг каждого издательства
-- Неверный результат
SELECT * 
FROM [books]
WHERE [Price] IN (SELECT MIN([Price])
				  FROM [books]
				  GROUP BY [Izd])


-- Запрос в одну и ту же таблицу дважды
-- Псевдонимы нужны чтобы не было одинаковых названий таблиц
-- Результатом будет декартово произведение этих таблиц
-- 767 * 767 результатов (588289)
-- Не делайте так
SELECT * 
FROM [books] AS Books1, [books] AS Books2


-- Получаем название издательства, название его самой дешевой книги и ее цены
-- Используем подзапрос внутри FROM
-- Для корректной работы подзапроса безымянным колонкам нужно задать псевдонимы
-- Также необходимо задать псевдоним для таблицы, получаемой подзапросом
SELECT [books].[Izd], [Name], [MinPrice] 
FROM [books], (SELECT [Izd], MIN([Price]) AS [MinPrice]
			   FROM [books]
			   GROUP BY [Izd]) AS [MinPricesByIzd]
WHERE [books].[Izd] = [MinPricesByIzd].[Izd] AND [books].[Price] = [MinPricesByIzd].[MinPrice]


-- Получаем название издательства, название его самой дешевой книги и ее цены
-- Пример без [] и с другим использованием псевдонимов
SELECT Izd, Name, Price
FROM books, (SELECT Izd AS ResultIzd, MIN(Price) AS MinPrice
			 FROM books
			 GROUP BY Izd) AS result
WHERE Izd = ResultIzd AND Price = MinPrice


-- Получаем название издательства, название книги и ее цены
-- цены которых ниже, чем цены всех книг от DiaSoft
SELECT Izd, Name, Price
FROM books
WHERE Price < ALL(SELECT Price FROM books WHERE Izd = 'DiaSoft')
ORDER BY Price DESC


-- Получаем название издательства, название книги и ее цены
-- цены которых ниже, чем минимальная цена книги от DiaSoft
-- Такой вариант более оптимальный чем верхний
SELECT Izd, Name, Price
FROM books
WHERE Price < (SELECT MIN(Price) FROM books WHERE Izd = 'DiaSoft')


-- Получаем название издательства, название книги и ее цены
-- цены которых ниже, чем цена любой из книг от DiaSoft
SELECT Izd, Name, Price
FROM books
WHERE Price < ANY(SELECT Price FROM books WHERE Izd = 'DiaSoft')
ORDER BY Price DESC


-- Получаем название издательства, название книги и ее цены
-- цены которых ниже, чем цена любой из книг от Gleb
-- В случае указания названия издательства, у которого нет ни одной книги
-- засчет операции EXISTS мы увидим все книги
SELECT Izd, Name, Price
FROM books
WHERE NOT EXISTS(SELECT Price FROM books WHERE Izd = 'Gleb') OR Price < ANY(SELECT Price FROM books WHERE Izd = 'Gleb')
ORDER BY Price DESC