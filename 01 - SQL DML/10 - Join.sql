-- Запросы в данном примере к БД LibrarySQL, которую вы можете
-- найти в репозитории и подключить в SSMS.

-- Убедитесь, что в SSMS у вас выбрана база данных LibrarySQL.
-- LibrarySQL - База данных библиотеки университета


-- Поучить данные из таблиц книг и авторов
-- Результатом будет декартово произведение двух таблиц
-- Так делать нельзя
SELECT *
FROM Books, Authors
WHERE Books.Id_Author = Authors.Id


-- Поучить данные из таблиц книг и соответствующих им авторов
-- При помощи условия WHERE мы делаем так, чтобы в комбинации
-- с книгами выходили только относящиеся к ним авторы,
-- таким образом предотвращая произведение двух таблиц
SELECT *
FROM Books, Authors
WHERE Books.Id_Author = Authors.Id


-- Добавим к книгам авторов и издательства
SELECT *
FROM Books, Authors, Press
WHERE Books.Id_Author = Authors.Id AND Books.Id_Press = Press.Id


-- Идентичный верхнему пример, но с использованием псевдонимов
SELECT *
FROM Books AS B, Authors AS A, Press AS P
WHERE B.Id_Author = A.Id AND B.Id_Press = P.Id


-- Пример присоединения таблиц через JOIN
-- Такой вариант является более предпочтительным
SELECT *
FROM Books
JOIN Authors ON Books.Id_Author = Authors.Id


-- Добавляем еще и таблицу издательств
SELECT *
FROM Books
JOIN Authors ON Books.Id_Author = Authors.Id
JOIN Press ON Books.Id_Press = Press.Id

-- Вывести название книги, имя и фамилию автора и название издательства 
SELECT Books.Name, Authors.FirstName, Authors.LastName, Press.Name
FROM Books
JOIN Authors ON Books.Id_Author = Authors.Id
JOIN Press ON Books.Id_Press = Press.Id


-- Объеднияем имя и фамилию автора и задаем псевдонимы
SELECT	Books.Name AS [Book name], 
		Authors.FirstName + ' ' + Authors.LastName as [Author name], 
		Press.Name as [Press name]
FROM Books
JOIN Authors ON Books.Id_Author = Authors.Id
JOIN Press ON Books.Id_Press = Press.Id


-- 4 вида JOIN:
-- JOIN
-- LEFT JOIN
-- RIGHT JOIN
-- FULL JOIN


-- Выводим сводную таблицу S_Cards (инфо о книгах, взятых в библиотеке) и студентах
-- Мы увидим только тех студентов, которые брали книги
SELECT * 
FROM S_Cards
JOIN Students ON S_Cards.Id_Student = Students.Id


-- Выводим сводную таблицу S_Cards (инфо о книгах, взятых в библиотеке) и студентах
-- Мы увидим только тех студентов, которые брали книги
SELECT * 
FROM Students
JOIN S_Cards ON S_Cards.Id_Student = Students.Id


-- Выводим сводную таблицу S_Cards (инфо о книгах, взятых в библиотеке) и студентах
-- Мы увидим только всех студентов в левой части таблицы
SELECT * 
FROM Students
LEFT JOIN S_Cards ON S_Cards.Id_Student = Students.Id


-- Выводим сводную таблицу S_Cards (инфо о книгах, взятых в библиотеке) и студентах
-- Мы увидим только всех студентов в правой части таблицы
SELECT * 
FROM S_Cards
RIGHT JOIN Students ON S_Cards.Id_Student = Students.Id


-- Выводим сводную таблицу S_Cards (инфо о книгах, взятых в библиотеке) и студентах
-- Мы увидим только всех студентов в правой части таблицы и все S_Cards в левой
SELECT * 
FROM S_Cards
FULL JOIN Students ON S_Cards.Id_Student = Students.Id