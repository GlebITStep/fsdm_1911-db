-- Виды объединения таблиц
-- UNION / UNION ALL
-- EXCEPT
-- INTERSECT


-- Для начала добавим в таблицу учителей человека, который есть в таблице студентов
INSERT INTO Teachers
VALUES ('Ольга', 'Хренова', 2)


-- Выведем вместе и студентов и учителей
-- Ольга Хренова будет в таблице 1 раз
-- несмотря на то, что она присутствует в двух таблицах
-- UNION убирает повторения
SELECT FirstName, LastName
FROM Students
UNION
SELECT FirstName, LastName
FROM Teachers


-- Выведем вместе и студентов и учителей
-- Ольга Хренова будет в таблице 2 раза
-- UNION ALL не убирает повторения
SELECT FirstName, LastName
FROM Students
UNION ALL
SELECT FirstName, LastName
FROM Teachers


-- Выведем тех людей, которые являются и студентами и преподавателями
-- Увидим только Ольгу Хренову
SELECT FirstName, LastName
FROM Students
INTERSECT
SELECT FirstName, LastName
FROM Teachers


-- Выведем тех людей, которые являются и студентами но не преподавателями
-- Увидим всех студентов, кроме Ольги Хреновой
SELECT FirstName, LastName
FROM Students
EXCEPT
SELECT FirstName, LastName
FROM Teachers
