-- Получим всех студентов, которые брали книги в библиотеке
-- Первый способ через JOIN
SELECT DISTINCT Students.FirstName, Students.LastName
FROM S_Cards
JOIN Students ON S_Cards.Id_Student = Students.Id
ORDER BY FirstName


-- Получим всех студентов, которые брали книги в библиотеке
-- Второй способ через подзапрос и EXISTS
SELECT FirstName, LastName
FROM Students
WHERE EXISTS(SELECT * FROM S_Cards WHERE Id_Student = Students.Id)
ORDER BY FirstName


-- Следуюшая задача - вывести самые объемные книги каждого издательтва
-- Мы должны получить: название книги, количество страниц, название издательства


-- Для начала найдем максимальное количество страниц в книге для каждого издательства
SELECT Id_Press, MAX(Pages)
FROM Books
GROUP BY Id_Press


-- Присоединим таблицу издательств для того, чтобы вывести название вместо Id
-- Тут мы попали в тупик, так как название книги вывести не получится 
SELECT Press.Name, MAX(Books.Pages)
FROM Books
JOIN Press ON Books.Id_Press = Press.Id
GROUP BY Press.Name


-- Попробуем решить проблему при помощи JOINа в таблицу, возвращаемую подзапросом
SELECT Books.Name, MaxPress.MaxPages, MaxPress.Id_Press
FROM Books
JOIN (SELECT Id_Press, MAX(Pages) AS MaxPages
	  FROM Books
	  GROUP BY Id_Press) AS MaxPress ON Books.Id_Press = MaxPress.Id_Press AND Books.Pages = MaxPress.MaxPages


-- Добавим еще один JOIN, при помощи которого получим название издательства 
SELECT Books.Name, MaxPages, Press.Name
FROM Books
JOIN (SELECT Id_Press AS MaxPressId, MAX(Pages) AS MaxPages
	  FROM Books
	  GROUP BY Id_Press) AS result ON Id_Press = MaxPressId AND Pages = MaxPages
JOIN Press ON MaxPressId = Press.Id