-- Точные числа
BIT  
TINYINT
SMALLINT
INT
BIGINT
DECIMAL
NUMERIC
SMALLMONEY
MONEY


-- Числа с плавающей запятой
FLOAT
REAL


-- Дата и время
DATE
TIME
DATETIME2
DATETIMEOFFSET
-- DATETIME (УСТАРЕЛО!!!)
-- SMALLDATETIME (УСТАРЕЛО!!!)


-- Строки
CHAR
NCHAR
VARCHAR
NVARCHAR
-- TEXT (УСТАРЕЛО!!!)
-- NTEXT (УСТАРЕЛО!!!)


-- Бинарные данные
BINARY
VARBINARY
-- IMAGE (УСТАРЕЛО!!!)