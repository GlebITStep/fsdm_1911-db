-- Создать таблицу
CREATE TABLE Positions
(
	Id INT, 
	Title NVARCHAR(30)
)

-- Типы ограничений и настроек
-- NULL
-- NOT NULL
-- UNIQUE
-- CHECK
-- DEFAULT
-- PRIMARY KEY
-- IDENTITY
-- REFERENCES


-- Создать таблицу с указанием ограничений и дополнительных параметров
CREATE TABLE Positions
(
	Id INT PRIMARY KEY IDENTITY, 
	Title NVARCHAR(30) NOT NULL UNIQUE
)


-- Создать таблицу с указанием именованных ограничений
CREATE TABLE Positions
(
	Id INT IDENTITY, 
	Title NVARCHAR(30) NOT NULL,

	CONSTRAINT PK_Positions_Id PRIMARY KEY (Id),
	CONSTRAINT UQ_Positions_Title UNIQUE (Title)
)


-- Создать таблицу с внешним ключем на другую таблицу
CREATE TABLE Employees
(
	Id INT PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(30) NOT NULL,
	Surname NVARCHAR(30) NOT NULL, 
	PositionId INT NOT NULL REFERENCES Positions(Id)


-- Создать таблицу с внешним ключем на другую таблицу (второй вариант)
CREATE TABLE Employees
(
	Id INT IDENTITY,
	[Name] NVARCHAR(30) NOT NULL,
	Surname NVARCHAR(30) NOT NULL, 
	PositionId INT NOT NULL,

	CONSTRAINT PK_Employees_Id PRIMARY KEY (Id),
	CONSTRAINT FK_Employees_PositionId FOREIGN KEY (PositionId) REFERENCES Positions(Id),
)


-- ON DELETE / ON UPDATE опции:
-- NO ACTION 
-- CASCADE
-- SET NULL
-- SET DEFAULT


-- Создать таблицу с внешним ключем с указанием ON DELETE
CREATE TABLE Employees
(
	Id INT IDENTITY,
	[Name] NVARCHAR(30) NOT NULL,
	Surname NVARCHAR(30) NOT NULL, 
	PositionId INT NOT NULL,

	CONSTRAINT PK_Employees_Id PRIMARY KEY (Id),
	CONSTRAINT FK_Employees_PositionId FOREIGN KEY (PositionId) REFERENCES Positions(Id) ON DELETE CASCADE
)


-- Удалить таблицу
DROP TABLE Positions


-- Добавить колонку в таблицу
ALTER TABLE Positions
ADD [Description] NVARCHAR(300)


-- Отредактировать колонку
ALTER TABLE Positions
ALTER COLUMN [Description] NVARCHAR(500)


-- Удалить колонку
ALTER TABLE Positions
DROP COLUMN [Description]


-- Добавить именованное ограничение (с разными ограничениями может быть разный синтаксис)
ALTER TABLE Positions 
DROP CONSTRAINT UQ_Positions_Title


-- Удалить ограничение
ALTER TABLE Positions
ADD CONSTRAINT UQ_Positions_Title UNIQUE (Title);


-- Переименовать базу данных
EXEC sp_renamedb 'SoftwareCompany', 'DeveloperCompany'


-- Переименовать таблицу
EXEC sp_rename 'Positions', 'Professions'


-- Переименовать колонку в таблице
EXEC sp_rename 'Positions.Description', 'About', 'COLUMN'


-- Переименовать ограничение
EXEC sp_rename 'UQ_Positions_Title', 'UQ_Positions_TitleNew', 'CONSTRAINT'