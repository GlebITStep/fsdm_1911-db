-- Create database
CREATE DATABASE SoftwareCompany
GO

-- Create schema
USE SoftwareCompany

CREATE TABLE Positions
(
	Id INT PRIMARY KEY IDENTITY,
	Title NVARCHAR(30) NOT NULL UNIQUE
)

CREATE TABLE Employees
(
	Id INT PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(30) NOT NULL CHECK([Name] NOT LIKE '%[0-9]%'),
	Surname NVARCHAR(30) NOT NULL CHECK(Surname NOT LIKE '%[0-9]%'), 
	PositionId INT NOT NULL DEFAULT(1) REFERENCES Positions(Id) ON DELETE SET DEFAULT
)

CREATE TABLE Projects
(
	Id INT PRIMARY KEY IDENTITY,
	Title NVARCHAR(30) NOT NULL UNIQUE,
	[Description] NVARCHAR(300)
)

CREATE TABLE ProjectEmployees
(
	Id INT PRIMARY KEY IDENTITY,
	ProjectId INT NOT NULL REFERENCES Projects(Id) ON DELETE CASCADE,
	EmployeeId INT NOT NULL REFERENCES Employees(Id) ON DELETE CASCADE
)

CREATE TABLE Phones
(
	Id INT PRIMARY KEY IDENTITY,	
	EmployeeId INT NOT NULL REFERENCES Employees(Id) ON DELETE CASCADE,
	Phone VARCHAR(20) NOT NULL CHECK(Phone NOT LIKE '%[a-z]%') UNIQUE,
)

CREATE TABLE Addresses
(
	Id INT PRIMARY KEY IDENTITY,	
	EmployeeId INT NOT NULL REFERENCES Employees(Id) ON DELETE CASCADE,
	PostCode CHAR(6),
	Country NVARCHAR(20) NOT NULL,
	City NVARCHAR(20) DEFAULT('Unknown'),
)

-- Insert data
INSERT INTO Positions VALUES (N'Unemployed')
INSERT INTO Positions VALUES (N'Software Engineer')
INSERT INTO Positions VALUES (N'Designer')

INSERT INTO Employees VALUES (N'Gleb', N'Skripnikov', 2)
INSERT INTO Employees VALUES (N'Firdovsi', N'Babatov', 3)
INSERT INTO Employees VALUES (N'Ivan', N'Ivan', 2)

INSERT INTO Phones VALUES (1, 111111)
INSERT INTO Phones VALUES (1, 222222)
INSERT INTO Phones VALUES (2, 333333)
INSERT INTO Phones VALUES (3, 444444)
INSERT INTO Phones VALUES (3, 555555)

INSERT INTO Projects VALUES (N'Bitwolf', N'Xamarin App')
INSERT INTO Projects VALUES (N'Hatity', N'Console App')
INSERT INTO Projects VALUES (N'Rank', N'Web App')

INSERT INTO ProjectEmployees VALUES (1, 1)
INSERT INTO ProjectEmployees VALUES (1, 2)
INSERT INTO ProjectEmployees VALUES (2, 1)
INSERT INTO ProjectEmployees VALUES (2, 3)
INSERT INTO ProjectEmployees VALUES (3, 3)

INSERT INTO Addresses VALUES (1, 'AZ1000', 'Azerbaijan', 'Baku')
INSERT INTO Addresses VALUES (2, '123123', 'Ukraine', 'Kiev')
INSERT INTO Addresses VALUES (3, '456456', 'Russia', 'Moscow')