-- Create database
CREATE DATABASE SoftwareCompany
GO

-- Create schema
USE SoftwareCompany

CREATE TABLE Positions
(
	Id INT IDENTITY,
	Title NVARCHAR(30) NOT NULL,
	
	CONSTRAINT PK_Positions_Id PRIMARY KEY (Id),
	CONSTRAINT UQ_Positions_Title UNIQUE (Title)
)

CREATE TABLE Employees
(
	Id INT IDENTITY,
	[Name] NVARCHAR(30) NOT NULL,
	Surname NVARCHAR(30) NOT NULL, 
	PositionId INT NOT NULL CONSTRAINT DF_Employees_PositionId DEFAULT(1),

	CONSTRAINT PK_Employees_Id PRIMARY KEY (Id),
	CONSTRAINT CK_Employees_Name CHECK([Name] NOT LIKE '%[0-9]%'),
	CONSTRAINT CK_Employees_Surname CHECK(Surname NOT LIKE '%[0-9]%'),
	CONSTRAINT FK_Employees_PositionId FOREIGN KEY (PositionId) REFERENCES Positions(Id) ON DELETE SET DEFAULT,
)

CREATE TABLE Projects
(
	Id INT IDENTITY,
	Title NVARCHAR(30) NOT NULL,
	[Description] NVARCHAR(300),

    CONSTRAINT PK_Projects_Id PRIMARY KEY (Id),
    CONSTRAINT UQ_Projects_Title UNIQUE (Title)
)

CREATE TABLE ProjectEmployees
(
	Id INT IDENTITY,
	ProjectId INT NOT NULL,
	EmployeeId INT NOT NULL,

    CONSTRAINT PK_ProjectEmployees_Id PRIMARY KEY (Id),
    CONSTRAINT FK_ProjectEmployees_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects(Id) ON DELETE CASCADE,
    CONSTRAINT FK_ProjectEmployees_EmployeeId FOREIGN KEY (EmployeeId) REFERENCES Employees(Id) ON DELETE CASCADE,
)

CREATE TABLE Phones
(
	Id INT IDENTITY,	
	EmployeeId INT NOT NULL,
	Phone VARCHAR(20) NOT NULL,

    CONSTRAINT PK_Phones_Id PRIMARY KEY (Id),
    CONSTRAINT FK_Phones_EmployeeId FOREIGN KEY (EmployeeId) REFERENCES Employees(Id) ON DELETE CASCADE,
    CONSTRAINT CK_Phones_Phone CHECK(Phone NOT LIKE '%[a-z]%'),
    CONSTRAINT UQ_Phones_Phone UNIQUE (Phone)
)

CREATE TABLE Addresses
(
	Id INT IDENTITY,	
	EmployeeId INT NOT NULL,
	PostCode CHAR(6),
	Country NVARCHAR(20) NOT NULL,
	City NVARCHAR(20) CONSTRAINT DF_Addresses_City DEFAULT('Unknown'),

    CONSTRAINT PK_Addresses_Id PRIMARY KEY (Id),
    CONSTRAINT FK_Addresses_EmployeeId FOREIGN KEY (EmployeeId) REFERENCES Employees(Id) ON DELETE CASCADE,
)

-- Insert data
INSERT INTO Positions VALUES (N'Unemployed')
INSERT INTO Positions VALUES (N'Software Engineer')
INSERT INTO Positions VALUES (N'Designer')

INSERT INTO Employees VALUES (N'Gleb', N'Skripnikov', 2)
INSERT INTO Employees VALUES (N'Firdovsi', N'Babatov', 3)
INSERT INTO Employees VALUES (N'Ivan', N'Ivan', 2)

INSERT INTO Phones VALUES (1, 111111)
INSERT INTO Phones VALUES (1, 222222)
INSERT INTO Phones VALUES (2, 333333)
INSERT INTO Phones VALUES (3, 444444)
INSERT INTO Phones VALUES (3, 555555)

INSERT INTO Projects VALUES (N'Bitwolf', N'Xamarin App')
INSERT INTO Projects VALUES (N'Hatity', N'Console App')
INSERT INTO Projects VALUES (N'Rank', N'Web App')

INSERT INTO ProjectEmployees VALUES (1, 1)
INSERT INTO ProjectEmployees VALUES (1, 2)
INSERT INTO ProjectEmployees VALUES (2, 1)
INSERT INTO ProjectEmployees VALUES (2, 3)
INSERT INTO ProjectEmployees VALUES (3, 3)

INSERT INTO Addresses VALUES (1, 'AZ1000', 'Azerbaijan', 'Baku')
INSERT INTO Addresses VALUES (2, '123123', 'Ukraine', 'Kiev')
INSERT INTO Addresses VALUES (3, '456456', 'Russia', 'Moscow')