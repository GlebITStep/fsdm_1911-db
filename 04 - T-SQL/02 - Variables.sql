-- Создание переменной, присваивание значения, вывод на экран

DECLARE @number INT
SET @number = 42

SELECT @number
PRINT(@number)


-- Пример использования переменной внутри запроса

DECLARE @year INT
SET @year = 2000

SELECT * 
FROM Books
WHERE YearPress = @year


-- Запрос, получающий самую старую книгу

SELECT *
FROM BOOKS 
WHERE YearPress = (SELECT MIN(YearPress) FROM Books)


-- Пример верхнего запроса с использованием переменной

DECLARE @minYear INT = (SELECT MIN(YearPress) FROM Books)

SELECT *
FROM BOOKS 
WHERE YearPress = @minYear


-- Еще один синтаксис записи результата запроса в переменные 

DECLARE @minYear INT
DECLARE @maxYear INT

SELECT @minYear = MIN(YearPress), @maxYear = MAX(YearPress) FROM Books

PRINT(@minYear)
PRINT(@maxYear)


-- Пример создания и работы с табличной переменной

DECLARE @testTable TABLE (Number INT, [Text] NVARCHAR(100))

INSERT INTO @testTable VALUES (1, 'One')
INSERT INTO @testTable VALUES (2, 'Two')

SELECT * FROM @testTable


-- Получить издательство, выпустившее наибольшее количество книг

SELECT Press.Name, COUNT(*) AS BooksCount
FROM Books
JOIN Press ON Id_Press = Press.Id
GROUP BY Press.Name 
HAVING COUNT(*) = (SELECT MAX(BooksCount) AS MaxBooksCount
				   FROM (SELECT Id_Press, COUNT(*) AS BooksCount
						 FROM Books
						 GROUP BY Id_Press) AS MaxPress) 



-- Пример верхнего запроса с использованием переменных

DECLARE @pressAndCount TABLE (Id_Press INT, BooksCount INT)

INSERT INTO @pressAndCount
SELECT Id_Press, COUNT(*) AS BooksCount
FROM Books
GROUP BY Id_Press 

DECLARE @maxBooks INT = (SELECT MAX(BooksCount) FROM @pressAndCount)

SELECT Press.Name, BooksCount
FROM @pressAndCount
JOIN Press ON Id_Press = Press.Id
WHERE BooksCount = @maxBooks