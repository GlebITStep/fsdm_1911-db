-- Пример использования конструкции IF ELSE

DECLARE @minYear INT = (SELECT MIN(YearPress) FROM Books)

IF @minYear = 1990
BEGIN
	PRINT('YES')
	SELECT * FROM Books WHERE YearPress = @minYear
END
ELSE IF @minYear = 1991
BEGIN
	PRINT('YES')
	SELECT * FROM Books WHERE YearPress = @minYear
END
ELSE
	PRINT('NO')


-- Пример использования конструкции WHILE

DECLARE @results TABLE ([Date] DATE, Amount MONEY) 
DECLARE @startDate DATE = GETDATE()
DECLARE @startMoney MONEY = 1000
DECLARE @months INT = 6
DECLARE @percent FLOAT = 1.5

WHILE @months > 0
BEGIN
	SET @months = @months - 1
	SET @startDate = DATEADD(MONTH, 1, @startDate)
	SET @startMoney = @startMoney + (@startMoney / 100 * 1.5)
	INSERT INTO @results VALUES (@startDate, ROUND(@startMoney, 2))
END

SELECT * FROM @results


-- Пример использования конструкции TRY CATCH 

DECLARE @PressName NVARCHAR(50) = N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In suscipit aliquam ex.'

BEGIN TRY
	INSERT INTO Press VALUES (@PressName)
END TRY
BEGIN CATCH
	PRINT('ERROR!')
	PRINT(ERROR_NUMBER())
	PRINT(ERROR_MESSAGE())
	PRINT(ERROR_SEVERITY())
	PRINT(ERROR_STATE())
END CATCH