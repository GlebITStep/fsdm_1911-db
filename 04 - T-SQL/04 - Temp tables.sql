-- Получить издательство, выпустившее наибольшее количество книг

SELECT Press.Name, COUNT(*) AS BooksCount
FROM Books
JOIN Press ON Id_Press = Press.Id
GROUP BY Press.Name 
HAVING COUNT(*) = (SELECT MAX(BooksCount) AS MaxBooksCount
				   FROM (SELECT Id_Press, COUNT(*) AS BooksCount
						 FROM Books
						 GROUP BY Id_Press) AS MaxPress) 


-- Пример верхнего запроса с использованием переменных

DECLARE @pressAndCount TABLE (Id_Press INT, BooksCount INT)

INSERT INTO @pressAndCount
SELECT Id_Press, COUNT(*) AS BooksCount
FROM Books
GROUP BY Id_Press 

DECLARE @maxBooks INT = (SELECT MAX(BooksCount) FROM @pressAndCount)

SELECT Press.Name, BooksCount
FROM @pressAndCount
JOIN Press ON Id_Press = Press.Id
WHERE BooksCount = @maxBooks


-- Пример верхнего запроса с использованием локальной временной таблицы

CREATE TABLE #pressAndCount (Id_Press INT, BooksCount INT)

INSERT INTO #pressAndCount
SELECT Id_Press, COUNT(*) AS BooksCount
FROM Books
GROUP BY Id_Press 

DECLARE @maxBooks INT = (SELECT MAX(BooksCount) FROM #pressAndCount)

SELECT Press.Name, BooksCount
FROM #pressAndCount
JOIN Press ON Id_Press = Press.Id
WHERE BooksCount = @maxBooks


-- Пример верхнего запроса с использованием глобальной временной таблицы

CREATE TABLE ##pressAndCount (Id_Press INT, BooksCount INT)

INSERT INTO ##pressAndCount
SELECT Id_Press, COUNT(*) AS BooksCount
FROM Books
GROUP BY Id_Press 

DECLARE @maxBooks INT = (SELECT MAX(BooksCount) FROM ##pressAndCount)

SELECT Press.Name, BooksCount
FROM ##pressAndCount
JOIN Press ON Id_Press = Press.Id
WHERE BooksCount = @maxBooks


-- Пример верхнего запроса с использованием вьюшки

CREATE VIEW pressAndCountView AS
SELECT Id_Press, COUNT(*) AS BooksCount
FROM Books
GROUP BY Id_Press 

SELECT Press.Name, BooksCount
FROM pressAndCountView
JOIN Press ON Id_Press = Press.Id
WHERE BooksCount = (SELECT MAX(BooksCount) FROM pressAndCountView)


-- Пример запроса с использованием CTE (WITH)
-- CTE - Common Table Expression

WITH PressAndBooks AS
(
	SELECT Id_Press, COUNT(*) AS BooksCount
	FROM Books
	GROUP BY Id_Press
)
SELECT MAX(BooksCount) FROM PressAndBooks