-- Синтаксис создания процедуры без параметров  

CREATE PROCEDURE Hello 
AS
PRINT 'Hello'


-- Выполнение процедуры 

EXEC Hello


-- Изменение процедуры

ALTER PROCEDURE Hello 
AS
PRINT 'Hello, world!!!'


-- Удаление процедуры

DROP PROCEDURE Hello


-- Создание процедуры с более чем одной строкой кода внутри

CREATE PROC Test
AS
BEGIN
	PRINT 'TEST'
	PRINT 'TEST2'
END


-- Создание процедуры для получения названия книг, их издательств и имен авторов

CREATE PROC BooksWithAutorsAndPress 
AS
BEGIN
    SELECT B.Name, P.Name, A.FirstName + ' ' + A.LastName AS Author
    FROM Books AS B
    JOIN Press AS P ON P.Id = Id_Press 
    JOIN Authors AS A ON A.Id = Id_Author 
END


-- Создание view для получения названия книг, их издательств и имен авторов

CREATE VIEW BooksWithAutorsAndPressView 
AS
SELECT B.Name AS Book, P.Name AS Press, A.FirstName + ' ' + A.LastName AS Author
FROM Books AS B
JOIN Press AS P ON P.Id = Id_Press 
JOIN Authors AS A ON A.Id = Id_Author 


-- Изменяем предыдущую процедуру для получения названия книг, их издательств и имен авторов с параметром

ALTER PROC BooksWithAutorsAndPress 
	@year INT
AS
BEGIN
    SELECT B.Name, P.Name, A.FirstName + ' ' + A.LastName AS Author
    FROM Books AS B
    JOIN Press AS P ON P.Id = Id_Press 
    JOIN Authors AS A ON A.Id = Id_Author 
    WHERE B.YearPress = @year
END


-- Изменяем предыдущую процедуру для получения названия книг, их издательств и имен авторов с дефолтным параметром

CREATE PROC BooksWithAutorsAndPress 
	@year INT = -1
AS
BEGIN
	IF @year = -1
	BEGIN
		SELECT B.Name, P.Name, A.FirstName + ' ' + A.LastName AS Author
		FROM Books AS B
		JOIN Press AS P ON P.Id = Id_Press 
		JOIN Authors AS A ON A.Id = Id_Author 
	END
	ELSE
	BEGIN 
		SELECT B.Name, P.Name, A.FirstName + ' ' + A.LastName AS Author
		FROM Books AS B
		JOIN Press AS P ON P.Id = Id_Press 
		JOIN Authors AS A ON A.Id = Id_Author 
		WHERE B.YearPress = @year
	END
END


-- Пример вызова последнего варианта процедуры 

EXEC BooksWithAutorsAndPress 1999
EXEC BooksWithAutorsAndPress 2000
EXEC BooksWithAutorsAndPress 2001
EXEC BooksWithAutorsAndPress


-- Процедура возвращающая значение

CREATE PROC SumTwoNumbers
	@first INT,
	@second INT
AS
BEGIN
	RETURN @first + @second
END


-- Пример вызова такой процедуры и записи ответа в переменную

DECLARE @result INT
EXEC @result = SumTwoNumbers 5, 10
PRINT @result


-- Процедура, возвращающая максимальное количество страниц

CREATE PROC MaxPages
AS
BEGIN
	DECLARE @max INT
	SELECT @max = MAX(Pages) FROM Books
	RETURN @max
END

DECLARE @result INT
EXEC @result = MaxPages
PRINT @result


-- Процедура, возвращающая два ответа через OUTPUT параметры

CREATE PROC MinMaxPages
	@min INT OUTPUT,
	@max INT OUTPUT
AS
BEGIN
	SELECT @max = MAX(Pages), @min = MIN(Pages) FROM Books
END

DECLARE @minResult INT = 0
DECLARE @maxResult INT = 0

EXEC MinMaxPages @minResult OUTPUT, @maxResult OUTPUT

PRINT @minResult
PRINT @maxResult


-- Процедура, увеличивающая количество книг в библиотеке по названию издательства

ALTER PROC IncrementQuantityByPressName
	@pressName NVARCHAR(30)
AS
BEGIN
	DECLARE @pressId INT

	SELECT @pressId = Id
	FROM Press
	WHERE [Name] = @pressName

	IF @pressId IS NOT NULL
	BEGIN
		UPDATE Books
		SET Quantity = Quantity + 1
		WHERE Id_Press = @pressId
	END
	ELSE 
		PRINT 'Press was not found!'
END

EXEC IncrementQuantityByPressName 'BHV'


-- Процедура, создающая студента и, при необходимости, группу

CREATE PROC CreateStudent
	@name NVARCHAR(15),
	@surname NVARCHAR(25),
	@groupName NVARCHAR(10)
AS
BEGIN
	DECLARE @groupId INT
	SELECT @groupId = Id FROM Groups WHERE [Name] = @groupName

	IF @groupId IS NULL
	BEGIN
		INSERT INTO Groups VALUES (@groupName, 1)
		SELECT @groupId = Id FROM Groups WHERE [Name] = @groupName
	END

	INSERT INTO Students VALUES (@name, @surname, @groupId, 1)
END

EXEC CreateStudent N'Глеб', N'Скрипников', N'FSDM_1911' 
EXEC CreateStudent N'Тест', N'Тест', N'FSDM_1911' 