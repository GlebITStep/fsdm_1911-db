-- Выполнение SQL запроса из строки

EXEC sp_executesql N'SELECT * FROM Books'


-- Процедура, позволяющая вывести все данные из таблицы, название которой передается в параметр

CREATE PROC GetAllRows
	@tableName NVARCHAR(100)
AS
BEGIN
	DECLARE @query NVARCHAR(250) = N'SELECT * FROM ' + @tableName
	EXEC sp_executesql @query
END

EXEC GetAllRows N'Books'


-- Процедура, выводящая TOP N данных отсортированных по колонке, переданной в качестве параметра

ALTER PROC GetTopBooksAndOrder
	@quantity INT,
	@column NVARCHAR(100),
	@desc BIT = 1
AS
BEGIN
	DECLARE @query NVARCHAR(250) = N'SELECT TOP ' 
		+ CAST(@quantity AS NVARCHAR(3)) 
		+ ' * FROM Books ORDER BY ' 
		+ @column + ' ' + IIF(@desc = 1, 'DESC', 'ASC')

	PRINT @query
	EXEC sp_executesql @query
END

EXEC GetTopBooksAndOrder 10, N'Pages', 0


-- Пример уязвимости SQL Injection в случае использования созданной выше процедуры

CREATE TABLE ImportantData
(
	Id INT PRIMARY KEY IDENTITY
)

EXEC GetTopBooksAndOrder 10, N'Pages; DROP TABLE ImportantData; --', 0 	-- произойдет DROP таблицы ImportantData
EXEC GetTopBooksAndOrder 10, N'Pages; SELECT * FROM Students; --', 0	-- произойдет выборка из таблицы Students помимо Books


-- Для предотвращения уязвимости все строковые параметры следует подставлять в запрос через функцию QUOTENAME

ALTER PROC GetTopBooksAndOrder
	@quantity INT,
	@column NVARCHAR(100),
	@desc BIT = 1
AS
BEGIN
	DECLARE @query NVARCHAR(250) = N'SELECT TOP ' 
		+ CAST(@quantity AS NVARCHAR(3)) 
		+ ' * FROM Books ORDER BY ' 
		+ QUOTENAME(@column) + ' ' + IIF(@desc = 1, 'DESC', 'ASC')

	PRINT @query
	EXEC sp_executesql @query
END

EXEC GetTopBooksAndOrder 10, N'Pages', 0
EXEC GetTopBooksAndOrder 10, N'Pages; DROP TABLE ImportantData; --', 0 	-- ошибка при выполнении
EXEC GetTopBooksAndOrder 10, N'Pages; SELECT * FROM Students; --', 0	-- ошибка при выполнении