-- Пример создания простейшей функции

CREATE FUNCTION TestFunction()
RETURNS INT
AS
BEGIN
	RETURN 0
END


-- Использование функции

SELECT dbo.TestFunction()


-- Изменение функции

ALTER FUNCTION TestFunction()
RETURNS INT
AS
BEGIN
	RETURN 999
END


-- Удаление функции

DROP FUNCTION dbo.TestFunction


-- Функция, сумирующая 2 числа

CREATE FUNCTION SumFunction(@first INT, @second INT)
RETURNS INT
AS
BEGIN
	RETURN @first + @second
END

SELECT dbo.SumFunction(4, 8)

SELECT [Name], dbo.SumFunction(Quantity, 10) FROM Books


-- Функция, для вывода количества товара в текстовом формате

CREATE FUNCTION TextQuantity(@quantity INT)
RETURNS NVARCHAR(20)
AS
BEGIN
	RETURN CASE 
		WHEN @quantity = 0 THEN 'Товар закончился'
		WHEN @quantity = 1 THEN 'Последний экземпляр'
		WHEN @quantity BETWEEN 2 AND 5 THEN 'Товар заканчивается'
		WHEN @quantity >= 6 THEN 'Товар в наличии'
		ELSE 'Нет данных'
	END
END

SELECT [Name], dbo.TextQuantity(Quantity) FROM Books


-- Функция для форматирования даты в текстовом формате

CREATE FUNCTION TextFormatDate(@date DATETIME)
RETURNS NVARCHAR(30)
AS
BEGIN
	RETURN CAST(DAY(@date) AS NVARCHAR(10)) + ' ' + DATENAME(MONTH, @date) + ' ' + CAST(YEAR(@date) AS NVARCHAR(10))
END

SELECT S.LastName, B.Name, dbo.TextFormatDate(C.DateOut)
FROM S_Cards AS C
JOIN Students AS S ON C.Id_Student = S.Id
JOIN Books AS B ON C.Id_Book = B.Id


-- Функция, для получения возраста по дате рождения и использование ее внутри ограничения CHECK

CREATE FUNCTION GetAgeByDateOfBirth(@date DATE)
RETURNS INT
AS 
BEGIN
	RETURN DATEDIFF(YEAR, @date, GETDATE())
END

CREATE TABLE Person
(
	Id INT PRIMARY KEY IDENTITY,
	FullName NVARCHAR(50) NOT NULL,
	DateOfBirth DATE NOT NULL,

	CONSTRAINT CK_Person_DateOfBirth CHECK(dbo.GetAgeByDateOfBirth(DateOfBirth) >= 18)
)

INSERT INTO Person VALUES (N'Gleb Skripnikov', '2010-01-16')


-- Пример функции, возвращающей таблицу

CREATE FUNCTION GetBooksByYear(@year INT)
RETURNS TABLE
AS
RETURN SELECT * FROM Books WHERE YearPress = @year

SELECT [Name] FROM dbo.GetBooksByYear(1999)


-- Пример функции возвращающей таблицу (второй вариант)

CREATE FUNCTION GetBooksWithAutrosAndPressByYear(@year INT)
RETURNS @result TABLE (Id INT, BookName NVARCHAR(100), PressName NVARCHAR(100), AuthorName NVARCHAR(100))
AS
BEGIN	
	INSERT INTO @result
	SELECT B.Id, B.Name, P.Name, A.LastName
	FROM Books AS B
	JOIN Authors AS A ON B.Id_Author = A.Id
	JOIN Press AS P ON B.Id_Press = P.Id
	WHERE YearPress = @year

	RETURN
END

SELECT * FROM dbo.GetBooksWithAutrosAndPressByYear(1999)



