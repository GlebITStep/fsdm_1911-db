-- Triggers INSERT UPDATE DELETE

-- Library DB

-- Создание триггера на вставку данных
CREATE TRIGGER TestTrigger
ON Students
AFTER INSERT
AS 
BEGIN
	PRINT 'New student added!'
END

-- Проверка работы триггера
INSERT INTO Students VALUES ('One', 'Two', 2, 1)

-- Удаление триггера
DROP TRIGGER TestTrigger


-- Создание триггера логирующего студентов, вставленых в таблицу
CREATE TRIGGER LogStudent
ON Students
AFTER INSERT 
AS 
BEGIN
	DECLARE @name NVARCHAR(50)
	DECLARE @surname NVARCHAR(50)
	SELECT @name = FirstName, @surname = LastName FROM inserted
	PRINT 'Student ' + @name + ' ' + @surname + ' added!'
END

-- Проверка триггера
INSERT INTO Students VALUES ('zxc', 'zxczxc', 2, 1)

-- Отключение триггера
ALTER TABLE Students
DISABLE TRIGGER LogStudent

-- Включение триггера
ALTER TABLE Students
ENABLE TRIGGER LogStudent




-- SocialNetwork DB


-- Добавим в таблицу постов счетчик комментариев 
ALTER TABLE Post
ADD CommentsCount INT NOT NULL DEFAULT 0

-- Добавим 2 поста
INSERT INTO Account VALUES ('test@test.test', 'qwerty')
INSERT INTO [Profile] VALUES (1, 'Test', 'Test', GETDATE(), 'test.jpg')
INSERT INTO Post VALUES (1, 'Post text', GETDATE(), 0)
INSERT INTO Post VALUES (1, 'Post text 2', GETDATE(), 0)

-- Проверяем посты
SELECT * FROM Post

-- Создаем триггер на добавление коммента
CREATE TRIGGER CommentCountTrigger
ON Comment
AFTER INSERT
AS
BEGIN
	DECLARE @postId INT
	SELECT @postId = PostId FROM inserted

	UPDATE Post
	SET CommentsCount = CommentsCount + 1
	WHERE Id = @postId
END

-- Создаем триггер на удаление коммента
CREATE TRIGGER DeleteCommentCountTrigger
ON Comment
AFTER DELETE
AS
BEGIN
	DECLARE @postId INT
	SELECT @postId = PostId FROM deleted

	UPDATE Post
	SET CommentsCount = CommentsCount - 1
	WHERE Id = @postId
END

-- Создаем триггер на изменение коммента
CREATE TRIGGER UpdateCommentCountTrigger
ON Comment
AFTER UPDATE
AS
BEGIN
	DECLARE @oldPostId INT
	SELECT @oldPostId = PostId FROM deleted

	DECLARE @newPostId INT
	SELECT @newPostId = PostId FROM inserted

	IF @oldPostId != @newPostId
	BEGIN
		UPDATE Post
		SET CommentsCount = CommentsCount - 1
		WHERE Id = @oldPostId

		UPDATE Post
		SET CommentsCount = CommentsCount + 1
		WHERE Id = @newPostId
	BEGIN
END

-- Триггер на блокировку вставки комментов
CREATE TRIGGER BlockInsert
ON Comment
INSTEAD OF INSERT
AS
BEGIN
	PRINT 'Insert to comments is denied!'
END

-- Триггер, заменяющий попытку удаления на изменение поля Deleted
CREATE TRIGGER SetDeleted
ON Comment
INSTEAD OF DELETE
AS
BEGIN
	DECLARE @Id INT
	SELECT @Id = Id FROM deleted

	UPDATE Comment
	SET Deleted = 1
	WHERE Id = @Id
END