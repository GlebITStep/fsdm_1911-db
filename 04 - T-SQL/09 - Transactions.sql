-- TCL (Transaction Control Language)


-- Создадим базу данных банка и добавим туда 2 аккаунта
CREATE DATABASE BankDb
CREATE TABLE Account
(
	Id UNIQUEIDENTIFIER PRIMARY KEY,
	Amount MONEY NOT NULL DEFAULT 0,

	CONSTRAINT CK_Amount_Account CHECK(Amount >= 0 AND Amount <= 10000)
)
INSERT INTO Account VALUES (NEWID(), 5000)
INSERT INTO Account VALUES (NEWID(), 9500)


-- Выведем результат на экран
SELECT * FROM Account


-- Запускаем транзакцию перевода денег с одного аккаунта на другой
-- В случае ошибки транзакция откатится на изначальное состояние
BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @amount MONEY = 1000

		UPDATE Account
		SET Amount = Amount - @amount
		WHERE Id = '595007D4-A3A1-4D26-BEBB-5B5A362C73AC'

		UPDATE Account
		SET Amount = Amount + @amount
		WHERE Id = '66258903-3A0A-4B6F-B52D-75467A2D2F29'

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
	END CATCH


-- Transaction isolation levels
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET TRANSACTION ISOLATION LEVEL SNAPSHOT