-- DCL (Data Control Language)

-- Создать логин test c паролем test
CREATE LOGIN test WITH PASSWORD = 'test'

-- Создать юзера TestUser и привязать его к логину test
CREATE USER TestUser FOR LOGIN test



-- GRANT DENY REVOKE

-- Дать пользователю доступ на чтение таблицы
-- Помимо SELECT тут также может быть INSERT, UPDATE, DELETE, ALTER, DROP, CREATE
GRANT SELECT ON Books TO TestUser

-- Запретить пользователю чтение из таблицы
DENY SELECT ON Books TO TestUser

-- Откатить право пользователя
REVOKE SELECT ON Books TO TestUser



-- Создать роль TestRole
CREATE ROLE TestRole

-- Привязать юзера к созданной роли
ALTER ROLE TestRole ADD MEMBER TestUser

-- Дать роли право на чтение всех таблиц
GRANT SELECT TO TestRole

-- Убрать роль у юзера
ALTER ROLE TestRole DROP MEMBER TestUser

-- Добавить юзера к стандартной роли db_datareader
ALTER ROLE db_datareader ADD MEMBER TestUser

-- Добавить юзера к стандартной роли db_datawriter
ALTER ROLE db_datawriter ADD MEMBER TestUser









