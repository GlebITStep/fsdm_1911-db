-- PIVOT позволяет развернуть данные в выборке

--Обычный вид
SELECT Press.Name, YearPress, SUM(Pages) 
FROM Books
JOIN Press ON Id_Press = Press.Id
GROUP BY Press.Name, YearPress

--Развернутый вид
SELECT Press.Name, [1990], [1999], [2000], [2001]
FROM (SELECT Id_Press, YearPress, Pages FROM Books) AS B
PIVOT ( 
	SUM(Pages) FOR YearPress IN ([1990], [1999], [2000], [2001]) 
) AS PivotTable
JOIN Press ON Id_Press = Press.Id




-- BULK INSERT позволяет импортировать в БД большие данные из файла

-- Создаем таблицу Person
CREATE TABLE Person
(
	Id INT PRIMARY KEY IDENTITY,
	FirstName NVARCHAR(20),
	LastName NVARCHAR(20),
	Age INT
)

-- Создаем View под эту таблицу - она понадобится для генерации Id во вставляемых данных
CREATE VIEW PrersonView AS
SELECT FirstName, LastName, Age
FROM Person

-- Делаем вставку данных их CSV файла
BULK INSERT PrersonView
FROM 'C:\Projects\Step\FSDM_1911\Data.csv'
WITH
(
	FIELDTERMINATOR = ',', -- Сепаратор полей в файле
	ROWTERMINATOR = '\n', -- Сепаратор строк в файле 
	FIRSTROW = 2 -- С какой строки начать считывание данных
)




-- MERGE позовляет синхронизировать две таблицы друг с другом

-- Создадим копию таблицы Person
SELECT *
INTO PersonCopy
FROM Person

-- Удалим из копии пару записей
DELETE FROM PersonCopy
WHERE Id = 2 OR Id = 4

-- Отредактируем одну из записей
UPDATE PersonCopy
SET Age = 99
WHERE Id = 1

-- Добавим новую запись
INSERT INTO PersonCopy 
VALUES ('Gleb', 'Skripnikov', 26)

--При помощи MERGE синхронизируем данные таблицы PersonCopy с табицей Person
MERGE PersonCopy AS Target 
USING Person AS Source
ON (Target.Id = Source.Id)

WHEN MATCHED 
THEN UPDATE SET 
	Target.FirstName = Source.FirstName,
	Target.LastName = Source.LastName,
	Target.Age = Source.Age

WHEN NOT MATCHED BY Target 
THEN INSERT VALUES (Source.FirstName, Source.LastName, Source.Age)

WHEN NOT MATCHED BY Source 
THEN DELETE;