//Для запуска примеров необходимо создать в Compass базу данных testdb 
//и добавить туда 3 коллекции: users, products и countries.
//Данные для заполнения этих коллекций находятся в этой же папке.

//Примеры необходимо запускать либо в Mongo shell через командную строку, 
//либо через расширение для VS Code "MongoDB for VS Code"


//Подключаемся к базе данных testdb
use('testdb'); //use testdb в Mongo shell


//SELECT * FROM users
db.users.find({});


//SELECT * FROM users WHERE gender = 'Female'
db.users.find({ gender: "Female" })


//SELECT * 
//FROM users 
//WHERE first_name IN ('Jamil', 'Daisi')
db.users.find(
{ 
    first_name: { 
        $in: ["Jamil", "Daisi"] 
    } 
});


//SELECT * 
//FROM users 
//WHERE age > 90
db.users.find({ age: { $gt: 90 } });


//$gt   >
//$lt   <
//$gte  >=
//$lte  <=


//SELECT * 
//FROM users 
//WHERE age > 90 AND gender = 'Female'
db.users.find({ gender: "Female", age: { $gte: 90 } });


//SELECT * 
//FROM users 
//WHERE age > 95 OR gender = 'Male'
db.users.find(
{ 
    $or: [ 
        { gender: "Male" }, 
        { age: { $gte: 95 } } 
    ] 
});


//SELECT * 
//FROM users 
//WHERE NOT age >= 95
db.users.find({ age: { $not: { $gte: 95 } } });


//SELECT * 
//FROM users 
//WHERE first_name LIKE 'A%'
db.users.find({ first_name: /^A/ });


//SELECT * 
//FROM users 
//WHERE first_name LIKE '%a'
db.users.find({ first_name: /A$/ });


//SELECT * 
//FROM users 
//WHERE first_name LIKE '[A-B]%'
db.users.find({ first_name: /^[A-B]/ });


//Забрать всех юзеров, чьи имена соответствуют регулярному выражению
db.users.find({ first_name: /^[A-B]{1}[a-z]{2,4}$/ });


//Найти все товары с точными размерами во вложеном объекте size
db.products.find(
{ 
    size: { 
        h: 14, 
        w: 21, 
        uom: "cm" 
    } 
});


//Найти все товары, размеры которых указаны в сантиметрах
db.products.find({ "size.uom": "cm" });


//Нати все товары с размерами в сантиметрах и высотой более 12
db.products.find({ "size.uom": "cm", "size.h": { $gt: 12 } });


//Найти все страны, имеющие границы ТОЛЬКО с Францией и Испанией
db.countries.find({ borders: ["FRA", "ESP"] });
//or
db.countries.find({ borders: { $all: ["FRA", "ESP"] } });


//Найти все старны, граничащие либо с Францией, либо с Испанией
db.countries.find({ borders: { $in: ["FRA", "ESP"] } });


//Найти все страны, граничащие с Францией и Германией
db.countries.find({ $and: [ { borders: { $in: ['FRA'] } }, { borders: { $in: ['DEU'] } } ] });


//Найти все старны, где используется валюта Евро, и вывести только demonym и area этих стран
db.countries.find({ currency: ["EUR"] }, { demonym: 1, area: 1 });


//То же, что и выше, но отключить выведение поля _id
db.countries.find({ currency: ["EUR"] }, { demonym: 1, area: 1, _id: 0 });


//Тот же запрос с добавлением сортировки по area по возрастанию
db.countries
    .find({ currency: ["EUR"] }, { demonym: 1, area: 1, _id: 0 })
    .sort({area: 1}) //-1 по убыванию


//Тот же запрос с пропуском первых 5 элементов выброки и выводом следующих 10
db.countries
    .find({ currency: ["EUR"] }, { demonym: 1, area: 1, _id: 0 })
    .sort({ area: 1 })
    .skip(5)
    .limit(10);




