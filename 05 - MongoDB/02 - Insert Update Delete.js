//Для запуска примеров необходимо создать в Compass базу данных testdb 
//и добавить туда 3 коллекции: users, products и countries.
//Данные для заполнения этих коллекций находятся в этой же папке.

//Примеры необходимо запускать либо в Mongo shell через командную строку, 
//либо через расширение для VS Code "MongoDB for VS Code"


use('testdb');


//FIND
db.products.find({});


//FIND ONE
db.products.findOne({ qty: { $gt: 50 } });


//INSERT ONE
db.products.insertOne({  
  item: "Bread",
  qty: 10, 
  status: "A",
  size: { w: 10, h: 20, uom: "cm" }
});



//INSERT MANY
db.products.insertMany([ 
  {  
    item: "Tea",
    qty: 20, 
    size: { w: 10, h: 20, l: 30, uom: "cm" },
    tags: [ "one", "two" ]
  }, 
  {  
    name: "Gleb",
    age: 26
  }
]);



//DELETE ONE
db.products.deleteOne({ name: "Gleb" });
//or
db.products.findOneAndDelete({ name: "Gleb" });


//DELETE MANY
db.products.deleteMany({ qty: { $lt: 30 } });


//UPDATE ONE
db.products.updateOne(
  { item: "Bread" },
  {
    $set: { qty: 10, tags: ["one", "two"] },
    $unset: { status: "" }
  }
)


//UPDATE MANY
db.products.updateMany(
  { qty: { $lt: 30 } },
  {
    $set: { qty: 30 }
  }
)


//REPLACE ONE
db.products.replaceOne(
  { item: "Tea" },
  { item: "Coffee", qty: 20, status: "A" }
)


//UPSERT (UPDATE OR INSERT)
db.products.replaceOne(
  { item: "Tea" },
  { item: "Milk", qty: 30, status: "B" },
  { upsert: true }
)