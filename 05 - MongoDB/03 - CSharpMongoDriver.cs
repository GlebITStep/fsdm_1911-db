using System;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;

namespace ConsoleAppMongoDb
{
    public class User
    {
        public ObjectId _id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string ip_address { get; set; }
        public int age { get; set; }
    }

    class Program
    {
        static void Main()
        {
            //var client = new MongoClient();
            //var db = client.GetDatabase("testdb");
            //var collection = db.GetCollection<BsonDocument>("products");

            // var result = collection.Find("{}").ToList();
            // foreach (var data in result)
            // {
            //    Console.WriteLine(data);   
            // }

            // var filter = "{ status: 'A', qty: { $lt: 50 } }";
            // var result = collection.Find(filter).ToList();
            // foreach (var data in result)
            // {
            //    Console.WriteLine(data);
            // }

            // var builder = Builders<BsonDocument>.Filter;
            // var filter = builder.Eq("qty", 100);
            // var result = collection.Find(filter).ToList();
            // foreach (var data in result)
            // {
            //    Console.WriteLine(data);
            // }







            // var client = new MongoClient();
            // var db = client.GetDatabase("testdb");
            // var collection = db.GetCollection<User>("users");

            // var result = collection.Find("{}").ToList();
            // foreach (var user in result)
            // {
            //    Console.WriteLine(user.last_name);
            // }

            // var result = collection.Find("{ first_name: /^A/ }").ToList();
            // foreach (var user in result)
            // {
            //    Console.WriteLine(user.first_name);
            // }

            // var builder = Builders<User>.Filter;
            // var filter = builder.Gt(x => x.age, 95);
            // var result = collection.Find(filter).ToList();
            // foreach (var user in result)
            // {
            //    Console.WriteLine($"Name: {user.first_name}; Age: {user.age}" );
            // }

            // var builder = Builders<User>.Filter;
            // var filter = builder.Where(x => x.age > 95 && x.gender == "Male" && x.first_name.StartsWith("A"));
            // var result = collection.Find(filter).ToList();
            // foreach (var user in result)
            // {
            //    Console.WriteLine($"Name: {user.first_name}; Age: {user.age}");
            // }

            // var builder = Builders<User>.Filter;
            // var ageFilter = builder.Gt(x => x.age, 95);
            // var genderFilter = builder.Eq(x => x.gender, "Male");
            // var nameFilter = builder.Regex(x => x.first_name, BsonRegularExpression.Create("/^A/"));
            // //var filter = builder.And(ageFilter, genderFilter, nameFilter);
            // var filter = ageFilter & genderFilter & nameFilter;
            // var result = collection.Find(filter).ToList();
            // foreach (var user in result)
            // {
            //    Console.WriteLine($"Name: {user.first_name}; Age: {user.age}");
            // }

            // //INSERT
            // var user = new User
            // {
            //    first_name = "Gleb",
            //    last_name = "Skripnikov",
            //    age = 26,
            //    email = "gleb@gmail.com",
            //    ip_address = "192.168.0.1"
            // };
            // collection.InsertOne(user);

            // //DELETE
            // var result = collection.DeleteOne(Builders<User>.Filter.Eq(x => x.first_name, "Gleb"));
            // Console.WriteLine(result.DeletedCount);

            // //REPLACE
            // var user = new User
            // {
            //    _id = ObjectId.GenerateNewId(),
            //    first_name = "Gleb",
            //    last_name = "Skripnikov",
            //    age = 26,
            //    email = "gleb@gmail.com",
            //    ip_address = "192.168.0.1",
            //    gender = "Male"
            // };
            // var filter = Builders<User>.Filter.Eq(x => x.first_name, "Firdovsi");
            // collection.ReplaceOne(filter, user, new ReplaceOptions { IsUpsert = true });

            // //UPDATE
            // var filter = Builders<User>.Filter.Eq(x => x.first_name, "Gleb");
            // var updates = Builders<User>.Update.Set(x => x.age, Int32.MaxValue);
            // collection.UpdateOne(filter, updates);
        }
    }
}
