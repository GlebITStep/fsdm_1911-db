﻿using System;
using System.Diagnostics;
using StackExchange.Redis;

namespace ConsoleAppRedisTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");
            var db = redis.GetDatabase();

            ////INSERT / UPDATE
            //db.StringSet("key1", "value");

            //READ
            var value = db.StringGet("key1");
            Console.WriteLine(value);

            ////DELETE
            //db.KeyDelete("key1");
        }
    }
}
