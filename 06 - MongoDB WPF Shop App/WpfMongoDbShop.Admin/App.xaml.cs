﻿using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using SimpleInjector;
using WpfMongoDbShop.Admin.ViewModels;
using WpfMongoDbShop.Shared.Services;

namespace WpfMongoDbShop.Admin
{
    public partial class App : Application
    {
        public static Container Services { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            RegisterServices();
            Start<ProductListViewModel>();
        }

        private void RegisterServices()
        {
            Services = new Container();

            Services.RegisterSingleton<IMessenger, Messenger>();
            Services.RegisterSingleton<INavigationService, NavigationService>();
            Services.RegisterSingleton<IProductRepository, ProductRepository>();
            Services.RegisterSingleton<IDataCache, DataCache>();
            Services.RegisterSingleton<MainViewModel>();
            Services.RegisterSingleton<ProductListViewModel>();
            Services.RegisterSingleton<ProductEditorViewModel>();

            Services.Verify();
        }

        private void Start<T>() where T : ViewModelBase
        {
            var windowViewModel = Services.GetInstance<MainViewModel>();
            windowViewModel.CurrentViewModel = Services.GetInstance<T>();
            var window = new MainWindow { DataContext = windowViewModel };
            window.ShowDialog();
        }
    }
}
