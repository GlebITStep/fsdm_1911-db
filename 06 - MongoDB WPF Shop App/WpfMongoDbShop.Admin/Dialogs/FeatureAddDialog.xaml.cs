﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfMongoDbShop.Admin.Dialogs
{
    /// <summary>
    /// Interaction logic for FeatureAddDialog.xaml
    /// </summary>
    public partial class FeatureAddDialog : Window
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public FeatureAddDialog()
        {
            InitializeComponent();
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void OnAddClick(object sender, RoutedEventArgs e)
        {
            Name = nameTextBox.Text;
            Value = valueTextBox.Text;
            DialogResult = true;
            Close();
        }
    }
}
