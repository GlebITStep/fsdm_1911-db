﻿using WpfMongoDbShop.Shared.Models;

namespace WpfMongoDbShop.Admin.Messages
{
    class ProductEditMessage
    {
        public Product Product { get; set; }
    }
}
