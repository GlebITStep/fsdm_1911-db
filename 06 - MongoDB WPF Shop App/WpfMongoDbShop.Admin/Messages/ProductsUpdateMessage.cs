﻿using WpfMongoDbShop.Shared.Models;

namespace WpfMongoDbShop.Admin.Messages
{
    class ProductsUpdateMessage
    {
        public Product Product { get; set; }
    }
}
