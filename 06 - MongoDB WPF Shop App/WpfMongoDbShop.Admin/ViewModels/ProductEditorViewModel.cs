﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using WpfMongoDbShop.Admin.Dialogs;
using WpfMongoDbShop.Admin.Messages;
using WpfMongoDbShop.Shared.Models;
using WpfMongoDbShop.Shared.Services;
using WpfMongoDbShop.Shared.Tools;

namespace WpfMongoDbShop.Admin.ViewModels
{
    class ProductEditorViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IProductRepository _productRepository;
        private readonly IMessenger _messenger;
        private Product _productToEdit = null;

        public RelayCommand CancelCommand { get; set; }
        public RelayCommand AddCommand { get; set; }
        public RelayCommand EditCommand { get; set; }
        public RelayCommand AddTagCommand { get; set; }
        public RelayCommand AddFeatureCommand { get; set; }
        public RelayCommand<string> RemoveTagCommand { get; set; }

        private string _name;
        public string Name
        {
            get => _name;
            set => Set(ref _name, value);
        }

        private decimal _price;
        public decimal Price
        {
            get => _price;
            set => Set(ref _price, value);
        }

        private string _tag;
        public string Tag
        {
            get => _tag;
            set => Set(ref _tag, value);
        }

        private bool _isEditMode = false;
        public bool IsEditMode
        {
            get => _isEditMode;
            set => Set(ref _isEditMode, value);
        }
        public bool IsAddMode
        {
            get => !_isEditMode;
            set => Set(ref _isEditMode, !value);
        }

        private ObservableCollection<string> _tags = new ObservableCollection<string>();
        public ObservableCollection<string> Tags
        {
            get => _tags;
            set => Set(ref _tags, value);
        }

        private ObservableDictionary<string, string> _features = new ObservableDictionary<string, string>();
        public ObservableDictionary<string, string> Features
        {
            get => _features;
            set => Set(ref _features, value);
        }

        public ProductEditorViewModel(
            INavigationService navigationService,
            IProductRepository productRepository,
            IMessenger messenger)
        {
            _navigationService = navigationService;
            _productRepository = productRepository;
            _messenger = messenger;

            CancelCommand = new RelayCommand(Cancel);
            AddCommand = new RelayCommand(Add);
            EditCommand = new RelayCommand(Edit);
            AddTagCommand = new RelayCommand(AddTag);
            AddFeatureCommand = new RelayCommand(AddFeature);
            RemoveTagCommand = new RelayCommand<string>(RemoveTag);

            _messenger.Register<ProductEditMessage>(this, message =>
            {
                Name = message.Product.Name;
                Price = message.Product.Price;

                if (message.Product.Tags != null)
                    Tags = new ObservableCollection<string>(message.Product.Tags);

                if (message.Product.Features != null)
                    Features = new ObservableDictionary<string, string>(message.Product.Features);

                IsEditMode = true;
                _productToEdit = message.Product;
            });
        }

        private void RemoveTag(string tag)
        {
            Tags.Remove(tag);
        }

        private void Cancel()
        {
            _navigationService.NavigateTo<ProductListViewModel>();
            Clear();
        }

        private void AddFeature()
        {
            var dialog = new FeatureAddDialog();
            var result = dialog.ShowDialog();
            if (result == true)
            {
                var addResult = Features.TryAdd(dialog.Name, dialog.Value);
                if (!addResult)
                    MessageBox.Show("This key already exists!");
            }
        }

        private void AddTag()
        {
            Tags.Add(Tag);
            Tag = string.Empty;
        }

        private void Add()
        {
            var product = new Product
            {
                Name = Name,
                Price = Price,
                Tags = Tags.ToList()
            };
            _productRepository.Add(product);

            _messenger.Send(new ProductsUpdateMessage {Product = product});

            _navigationService.NavigateTo<ProductListViewModel>();
            Clear();
        }

        private void Edit()
        {
            _productToEdit.Name = Name;
            _productToEdit.Price = Price;
            _productToEdit.Tags = Tags.ToList();
            _productToEdit.Features = Features.ToDictionary(x => x.Key, x => x.Value);

            _productRepository.Replace(_productToEdit);
            _messenger.Send(new ProductsUpdateMessage { Product = _productToEdit });
            _navigationService.NavigateTo<ProductListViewModel>();
            Clear();
        }

        private void Clear()
        {
            Name = string.Empty;
            Price = 0;
            Tag = string.Empty;
            Tags.Clear();
            IsEditMode = false;
            Features.Clear();
        }
    }

}
