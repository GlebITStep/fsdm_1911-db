﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Navigation;
using AutoFixture;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using LoremNET;
using WpfMongoDbShop.Admin.Messages;
using WpfMongoDbShop.Shared.Models;
using WpfMongoDbShop.Shared.Services;

namespace WpfMongoDbShop.Admin.ViewModels
{
    class ProductListViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IProductRepository _productRepository;
        private readonly IMessenger _messenger;

        private ObservableCollection<Product> _products;
        public ObservableCollection<Product> Products
        {
            get => _products;
            set => Set(ref _products, value);
        }

        public RelayCommand AddCommand { get; set; }
        public RelayCommand<Product> EditCommand { get; set; }
        public RelayCommand<Product> DeleteCommand { get; set; }


        public ProductListViewModel(
            INavigationService navigationService, 
            IProductRepository productRepository,
            IMessenger messenger)
        {
            _navigationService = navigationService;
            _productRepository = productRepository;
            _messenger = messenger;

            AddCommand = new RelayCommand(AddProduct);
            EditCommand = new RelayCommand<Product>(EditProduct);
            DeleteCommand = new RelayCommand<Product>(DeleteProduct);

            LoadProducts();

            _messenger.Register<ProductsUpdateMessage>(this, message => LoadProducts());



            //var fixture = new Fixture();
            //fixture.Register<string>(() => Lorem.Words(1, 3));
            //for (int i = 0; i < 10; i++)
            //{
            //    Products.Add(fixture.Create<Product>());
            //}
        }


        private void DeleteProduct(Product product)
        {
            _productRepository.Remove(product.Id);
            LoadProducts();
        }

        private void EditProduct(Product product)
        {
            _messenger.Send(new ProductEditMessage { Product = product });
            _navigationService.NavigateTo<ProductEditorViewModel>();
        }

        private void AddProduct()
        {
            _navigationService.NavigateTo<ProductEditorViewModel>();
        }

        private void LoadProducts()
        {
            var productsFromDb = _productRepository.GetAll();
            Products = new ObservableCollection<Product>(productsFromDb);
        }
    }
}
