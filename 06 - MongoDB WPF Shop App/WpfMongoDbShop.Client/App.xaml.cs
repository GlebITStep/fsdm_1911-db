﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using SimpleInjector;
using WpfMongoDbShop.Client.ViewModels;
using WpfMongoDbShop.Shared.Services;
using INavigationService = WpfMongoDbShop.Shared.Services.INavigationService;
using NavigationService = WpfMongoDbShop.Shared.Services.NavigationService;

namespace WpfMongoDbShop.Client
{
    public partial class App : Application
    {
        public static Container Services { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            RegisterServices();
            Start<HomeViewModel>();
        }

        private void RegisterServices()
        {
            Services = new Container();

            Services.RegisterSingleton<IMessenger, Messenger>();
            Services.RegisterSingleton<INavigationService, NavigationService>();
            Services.RegisterSingleton<IProductRepository, ProductRepository>();
            Services.RegisterSingleton<IDataCache, DataCache>();
            Services.RegisterSingleton<MainViewModel>();
            Services.RegisterSingleton<HomeViewModel>();
            Services.RegisterSingleton<DetailsViewModel>();

            Services.Verify();
        }

        private void Start<T>() where T : ViewModelBase
        {
            var windowViewModel = Services.GetInstance<MainViewModel>();
            windowViewModel.CurrentViewModel = Services.GetInstance<T>();
            var window = new MainWindow { DataContext = windowViewModel };
            window.ShowDialog();
        }
    }
}
