﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfMongoDbShop.Shared.Models;

namespace WpfMongoDbShop.Client.Messages
{
    class ProductDetailsMessage
    {
        public string ProductId { get; set; }
    }
}
