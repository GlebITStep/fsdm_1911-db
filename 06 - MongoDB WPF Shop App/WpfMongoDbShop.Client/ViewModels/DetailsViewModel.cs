﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using WpfMongoDbShop.Client.Messages;
using WpfMongoDbShop.Shared.Models;
using WpfMongoDbShop.Shared.Services;

namespace WpfMongoDbShop.Client.ViewModels
{
    class DetailsViewModel : ViewModelBase
    {
        private readonly IMessenger _messenger;
        private readonly INavigationService _navigationService;
        private readonly IProductRepository _productRepository;

        public RelayCommand BackCommand { get; set; }

        private Product _product;
        public Product Product
        {
            get => _product;
            set => Set(ref _product, value);
        }

        public DetailsViewModel(
            IMessenger messenger, 
            INavigationService navigationService,
            IProductRepository productRepository)
        {
            _messenger = messenger;
            _navigationService = navigationService;
            _productRepository = productRepository;

            BackCommand = new RelayCommand(Back);

            _messenger.Register<ProductDetailsMessage>(this, message =>
            {
                Product = _productRepository.GetById(message.ProductId);
            });
        }

        private void Back()
        {
            _navigationService.NavigateTo<HomeViewModel>();
        }
    }
}
