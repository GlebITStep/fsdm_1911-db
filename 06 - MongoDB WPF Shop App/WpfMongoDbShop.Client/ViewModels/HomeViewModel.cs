﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using WpfMongoDbShop.Client.Messages;
using WpfMongoDbShop.Shared.Models;
using WpfMongoDbShop.Shared.Services;

namespace WpfMongoDbShop.Client.ViewModels
{
    class HomeViewModel : ViewModelBase
    {
        private readonly IMessenger _messenger;
        private readonly INavigationService _navigationService;
        private readonly IProductRepository _productRepository;

        public RelayCommand<Product> DetailsCommand { get; set; }

        private ObservableCollection<Product> _products = new ObservableCollection<Product>();
        public ObservableCollection<Product> Products
        {
            get => _products;
            set => Set(ref _products, value);
        }

        public HomeViewModel(
            IMessenger messenger,
            INavigationService navigationService,
            IProductRepository productRepository)
        {
            _messenger = messenger;
            _navigationService = navigationService;
            _productRepository = productRepository;


            DetailsCommand = new RelayCommand<Product>(Details);

            Products = new ObservableCollection<Product>(_productRepository.GetAll());
        }

        private void Details(Product product)
        {
            _messenger.Send(new ProductDetailsMessage { ProductId = product.Id });
            _navigationService.NavigateTo<DetailsViewModel>();
        }
    }
}
