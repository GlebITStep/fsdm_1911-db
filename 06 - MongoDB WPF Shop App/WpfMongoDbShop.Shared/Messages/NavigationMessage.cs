﻿using System;

namespace WpfMongoDbShop.Shared.Messages
{
    public class NavigationMessage
    {
        public Type ViewModelType { get; set; }
    }
}
