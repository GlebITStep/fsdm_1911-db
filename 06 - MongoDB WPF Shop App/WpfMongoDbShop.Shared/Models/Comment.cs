﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace WpfMongoDbShop.Shared.Models
{
    [BsonIgnoreExtraElements]
    public class Comment
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
    }
}