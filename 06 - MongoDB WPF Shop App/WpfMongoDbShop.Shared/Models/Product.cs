﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WpfMongoDbShop.Shared.Models
{
    [BsonIgnoreExtraElements]
    public class Product
    {
        [BsonId, BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public List<string> Tags { get; set; }
        public Dictionary<string, string> Features { get; set; }
        public List<Comment> Comments { get; set; }
    }
}
