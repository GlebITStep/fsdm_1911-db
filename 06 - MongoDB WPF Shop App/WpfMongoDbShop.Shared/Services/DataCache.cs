﻿using System;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using StackExchange.Redis;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace WpfMongoDbShop.Shared.Services
{
    public class DataCache : IDataCache
    {
        private readonly IDatabase db;

        public DataCache()
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");
            db = redis.GetDatabase();
        }

        public void Set<T>(string key, T obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            db.StringSet(key, json);
        }

        public T Get<T>(string key)
        {
            var json = db.StringGet(key);

            if (!json.HasValue)
                return default;

            var obj = JsonConvert.DeserializeObject<T>(json);
            return obj;
        }

        public void Remove(string key)
        {
            db.KeyDelete(key);
        }
    }
}