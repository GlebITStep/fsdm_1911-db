﻿using System.Collections.Generic;
using System.Text;

namespace WpfMongoDbShop.Shared.Services
{
    public interface IDataCache
    {
        void Set<T>(string key, T obj);
        T Get<T>(string key);
        void Remove(string key);
    }
}
