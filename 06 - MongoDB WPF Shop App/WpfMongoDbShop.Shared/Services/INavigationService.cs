﻿using GalaSoft.MvvmLight;

namespace WpfMongoDbShop.Shared.Services
{
    public interface INavigationService
    {
        void NavigateTo<T>() where T : ViewModelBase;
    }
}
