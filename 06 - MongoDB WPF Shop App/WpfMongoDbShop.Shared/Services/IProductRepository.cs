﻿using System.Collections.Generic;
using WpfMongoDbShop.Shared.Models;

namespace WpfMongoDbShop.Shared.Services
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
        Product GetById(string id);
        void Add(Product product);
        void Replace(Product product);
        void Remove(string id);
    }
}
