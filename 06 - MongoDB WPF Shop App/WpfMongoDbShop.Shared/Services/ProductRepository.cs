﻿using System.Collections.Generic;
using MongoDB.Driver;
using WpfMongoDbShop.Shared.Models;

namespace WpfMongoDbShop.Shared.Services
{
    public class ProductRepository : IProductRepository
    {
        private const string AllProductsKey = "products";

        private readonly IDataCache _dataCache;
        private readonly IMongoCollection<Product> _collection;

        public ProductRepository(IDataCache dataCache)
        {
            _dataCache = dataCache;
            var client = new MongoClient();
            var db = client.GetDatabase("shop");
            _collection = db.GetCollection<Product>("products");
        }

        public IEnumerable<Product> GetAll()
        {
            var products = _dataCache.Get<IEnumerable<Product>>(AllProductsKey);

            if (products is null)
            {
                products = _collection.Find("{}").ToList();
                _dataCache.Set(AllProductsKey, products);
            }

            return products;
        }

        public Product GetById(string id)
        {
            var key = AllProductsKey + $":{id}";
            var product = _dataCache.Get<Product>(key);

            if (product is null)
            {
                var filter = Builders<Product>.Filter.Eq(x => x.Id, id);
                product = _collection.Find(filter).FirstOrDefault();
                _dataCache.Set(key, product);
            }

            return product;
        }

        public void Add(Product product)
        {
            _collection.InsertOne(product);
            _dataCache.Remove(AllProductsKey);

            var key = AllProductsKey + $":{product.Id}";
            _dataCache.Remove(key);
        }

        public void Replace(Product product)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.Id, product.Id);
            _collection.ReplaceOne(filter, product);
            _dataCache.Remove(AllProductsKey);

            var key = AllProductsKey + $":{product.Id}";
            _dataCache.Remove(key);
        }

        public void Remove(string id)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.Id, id);
            _collection.DeleteOne(filter);
            _dataCache.Remove(AllProductsKey);

            var key = AllProductsKey + $":{id}";
            _dataCache.Remove(key);
        }
    }
}