// INSTALL CassandraCSharpDriver NUGET PACKAGE!

using System;
using Cassandra;

namespace ConsoleAppCassandra
{
    class Program
    {
        static void Main(string[] args)
        {
            var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
            var session = cluster.Connect("university");
            var rs = session.Execute("SELECT * FROM students");

            foreach (var row in rs)
            {
                Console.WriteLine(row.GetValue<int>("id"));
                Console.WriteLine(row.GetValue<string>("name"));
                Console.WriteLine(row.GetValue<string>("surname"));
                var grades = row.GetValue<int[]>("grades");
                foreach (var grade in grades)
                {
                    Console.Write($"{grade}, ");
                }
            }
        }
    }
}
