//ASCII-Grafics syntax
// ()
// []
// (:Person)-[ACTED_IN]->(:Movie)


// SELECT * FROM *
MATCH (n) RETURN n


// SELECT * FROM Person
MATCH (n:Person) RETURN n


// SELECT * FROM Person WHERE born = 1966
MATCH (n:Person {born:1966}) RETURN n


// SELECT * FROM Person WHERE born > 1966
MATCH (n:Person) 
WHERE n.born > 1966
RETURN n


// SELECT name, born FROM Person WHERE born > 1966
MATCH (n:Person) 
WHERE n.born > 1966
RETURN n.name, n.born


MATCH (p:Person {name: 'Keanu Reeves'})-[:ACTED_IN]->(m:Movie)
RETURN p, m


MATCH (p:Person {name:'Keanu Reeves'})-[:ACTED_IN {roles:['Neo']}]->(m:Movie)
RETURN p, m


MATCH (p:Person)-[a:ACTED_IN]->(m:Movie)
WHERE p.name = 'Keanu Reeves' AND a.roles = ['Neo'] AND m.released = 1999
RETURN p, m


MATCH (k:Person {name:'Keanu Reeves'})-[:ACTED_IN]->(m:Movie)<-[:ACTED_IN]-(p:Person)
RETURN k, m, p


MATCH 
    (k:Person {name:'Keanu Reeves'})-[:ACTED_IN]->(m:Movie),
    (p:Person)-[:ACTED_IN]->(m)
RETURN 
    p.name, p.born


MATCH 
    (t:Person {name:'Tom Hanks'})-[]->(m:Movie)
RETURN 
    t, m


CREATE (:Account {name:'Gleb'}), (:Account {name:'Alina'})


MATCH
    (a:Account {name:'Gleb'})
SET 
    a.age = 26


MATCH
    (g:Account {name:'Gleb'}), 
    (a:Account {name:'Alina'})
CREATE
    (g)-[:FOLLOWS {since: '01-01-2017'}]->(a)


MATCH
    (g:Account {name:'Gleb'})-[:FOLLOWS]-(a:Account)
RETURN
    g, a


MATCH
    (g:Account {name:'Gleb'}), 
    (a:Account {name:'Alina'})
DETACH DELETE  
    a, g